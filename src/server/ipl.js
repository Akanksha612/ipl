const { result } = require("underscore");


function matches(arr) {

  if (!Array.isArray(arr) || !arr)
    return;

  var obj = {};
  for (var i = 0; i < arr.length; i++) {
    if (obj.hasOwnProperty(arr[i].season)) {
      obj[arr[i].season] += 1;
    }

    else
      obj[arr[i].season] = 1;
  }


  return obj;


}


module.exports.matches = matches;

//-----------------------------------------------------------------------------------------


function matchesWonPerTeam(match)
{
  var obj={};
  for(var i=0;i<match.length;i++)
  {
    var team= match[i].winner;
    var year= match[i].season;

    if(obj.hasOwnProperty(team))
    {
      if(obj[team].hasOwnProperty(year))
      obj[team][year]+=1;

      else
      obj[team][year]=1;
    }

    else
    {
      obj[team]={[year] : 1};   //very important to put year in []
    }

  }

 return obj; 
}

module.exports.matchesWonPerTeam=matchesWonPerTeam;







//---------------------------------------------------------------------------------------------

function extra_runs(arr, array, year) {

  if (!Array.isArray(arr) || !arr || !Array.isArray(array) || !array)
    return;

  var ids = [];          //.includes ki complexity is high so use helper object ={id : year} so that O(1) to search for id 

  for (var i = 0; i < arr.length; i++) {
    if (arr[i].season == year) {
      ids.push(parseInt(arr[i].id));
    }


  }

  var obj = {};   // obj= {bowlingteam : extraruns given by it}



  for (var i = 0; i < array.length; i++) {
    if (ids.includes(parseInt(array[i].match_id)))
     {
      var key = array[i].bowling_team;

      if (obj.hasOwnProperty(key))
        obj[key] += parseInt(array[i].extra_runs);

      else
        obj[key] = parseInt(array[i].extra_runs);
    }
  }

  return obj;


}


module.exports.extra_runs = extra_runs;



//---------------------------------------------------------------------------------------------------

function economyBowlers(matches, deliveries, year) {

  if (!Array.isArray(matches) || !matches || !Array.isArray(deliveries) || !deliveries)
    return;


  var ids = [];

  for (var i = 0; i < matches.length; i++) {
    if (matches[i].season == year) {
      ids.push(matches[i].id);
    }


  }

  var obj = {};

  //obj= {bowler : {runs:___, balls:___,economy___}};


  for (var i = 0; i < deliveries.length; i++) {

    let matchId = (deliveries[i].match_id);

    if (ids.includes(matchId)) {
      var key = deliveries[i].bowler;

      if (obj.hasOwnProperty(key)) {
        obj[key].balls = obj[key].balls + 1;
        obj[key].runs = obj[key].runs + parseInt(deliveries[i].total_runs);
        obj[key].economy = (obj[key].runs / (obj[key].balls / 6)).toFixed(2);
      }
      else {
        obj[key] = { "runs": parseInt(deliveries[i].total_runs), "balls": 1, "economy": 0 };
      }
    }

  }


  //low economy is considered good (less runs given/ per over)

 // return Object.entries(obj).sort((a, b) => b[1].economy - a[1].economy).reverse().slice(0, 10);  //descending sort

 return Object.entries(obj).sort((a, b) => a[1].economy - b[1].economy).slice(0, 10);  // ascending sort




} //function end


module.exports.economyBowlers = economyBowlers;


//---------------------------------------------------------------------------------------------------



function tossAndWon(arr) {

  if (!arr)
    return;

  var obj = {};

  for (var i = 0; i < arr.length; i++) {
    if (arr[i].toss_winner === arr[i].winner) {
      var key = arr[i].toss_winner;
      if (obj.hasOwnProperty(key)) {
        obj[key] += 1;
      }

      else
        obj[key] = 1;
    }
  }

  return obj;
}

module.exports.tossAndWon = tossAndWon;



//---------------------------------------------------------------------------



function Player(arr) {

  if (!arr)
    return;

  var obj = {};
  var result = {};




  var season = 2017;

  for (var i = 0; i < arr.length; i++) {

    if (arr[i].season == season)
     {
      var key = arr[i].player_of_match;
      if (obj.hasOwnProperty(key))
        obj[key] += 1;

      else
        obj[key] = 1;
    }

    else {
      result[season] = Object.entries(obj).sort((a, b) => b[1] - a[1])[0];  //descending sort
      season = arr[i].season;
      obj = {};

      if (i == arr.length - 1) //edge case for which loop will not run
      {
        result[arr[i].season] = [arr[i].player_of_match, 1];

      }


    }

  }

  return result;

}



module.exports.Player = Player;


//------------------------------------------------------------------------------------

function strikerate(delivery,match) {

  if (!match || !delivery)
    return;

  var helper = {};  //helper= {id: season}

  for (var i = 0; i < match.length; i++) {
    helper[match[i].id] = match[i].season;
  }


  var result = {};  // result= { batsman1: {season1: {}, season2:{}} , batsman2: {season1:{}, season2:{}} }



  for (var i = 0; i < delivery.length; i++) 
  {
    var key = delivery[i]['batsman'];
    var season_key = helper[delivery[i]['match_id']];

    if (result.hasOwnProperty(key))   // if batsman exist in result
    {

      if (result[key].hasOwnProperty(season_key)) //batsman + season both exists
      {
        result[key][season_key].runs += parseInt(delivery[i].batsman_runs);
        result[key][season_key].balls += 1;
        result[key][season_key].strikeRate = (result[key][season_key].runs * 100) / result[key][season_key].balls;
      }
      else   //batsman exist, but season dont exist
      {
        result[key][season_key] = {"runs": parseInt(delivery[i].batsman_runs), "balls": 1, "strikeRate":delivery[i].batsman_runs100 };
      }

    } 

    else //current batsman dont exist in result
    {

              result[key]= {[season_key]: {"runs": parseInt(delivery[i].batsman_runs), "balls": 1, "strikeRate": parseFloat(delivery[i].batsman_runs)*100}};
   }

  } 

  return result;

}
module.exports.strikerate = strikerate;




//------------------------------------------------------------------------------------------------

function dismissed(delivery) {
  var obj = {};

  if (!delivery)
    return;

  for (var i = 0; i < delivery.length; i++) {
    var key = delivery[i].player_dismissed;

    if (!key)
      continue;

    else if (obj.hasOwnProperty(key)) {
      obj[key] += 1;
    }

    else
      obj[key] = 1;
  }

  return Object.entries(obj).sort((a, b) => b[1] - a[1])[0];

}


module.exports.dismissed = dismissed;


//------------------------------------------------------------------------------------------------


function superOverEconomy(delivery) {

  if (!delivery)
    return;

  var obj = {};

  for (var i = 0; i < delivery.length; i++) {
    var match = delivery[i];
    if (obj.hasOwnProperty(match.bowler)) {

      if (match.is_super_over == '1') {
        obj[match.bowler].balls += 1;
        obj[match.bowler].runs += parseInt(match.total_runs);

        obj[match.bowler].economy = (obj[match.bowler].runs / (obj[match.bowler].balls / 6)).toFixed(2);
      }
    }
    else {
      if (match.is_super_over == '1') {
        obj[match.bowler] = { "runs": parseInt(match.total_runs), "balls": 1, "economy": 0 };
      }
    }


  }



  return Object.entries(obj).sort((a, b) => a[1].economy - b[1].economy)[0];


}


module.exports.superOverEconomy = superOverEconomy;


//-------------------------------------------------------------------------------------------------------------

/*
function matchesWonPerTeamGraph(match)                     //match = [{}, {}, {}]
{
  var result={};                                   //result= { year1: {t1:  , t2:  ,t3: },  year2: {t1:  ,t2:  , t3: }}

  for(var i=0;i<match.length;i++)
  {
    if(result.hasOwnProperty(match[i].season))
    {
      if(result[match[i].season][match[i].winner]) //season + winner both exist
      {
        result[match[i].season][match[i].winner]+=1;
      }

      else //season exist,winner don't
      {
           result[match[i].season][match[i].winner]=1;
      }
    }

    else   //season dont exist
    {
        result[match[i].season] = { [match[i].winner] : 1} ;
      
    }
  } 

  //return result;

 }

module.exports.matchesWonPerTeamGraph=matchesWonPerTeamGraph;

*/


//-----------------------------------------------------------------------------------------

function topBowlersGraph(matches, deliveries, year)
{

  var ids = [];

  for (var i = 0; i < matches.length; i++) 
  {
    if (matches[i].season == year) 
    {
      ids.push(parseInt(matches[i].id));
    }


  }

  var obj = {};

  //obj= {bowler : {runs:___, balls:___,economy___}};


  for (var i = 0; i < deliveries.length; i++)
   {

    let matchId = parseInt(deliveries[i].match_id);

    if (ids.includes(matchId))
     {
      var key = deliveries[i].bowler;

      if (obj.hasOwnProperty(key)) 
      {
        obj[key].balls = obj[key].balls + 1;
        obj[key].runs = obj[key].runs + parseInt(deliveries[i].total_runs);
        obj[key].economy = (obj[key].runs / (obj[key].balls / 6)).toFixed(2);
      }
      else 
      {
        obj[key] = { "runs": parseInt(deliveries[i].total_runs), "balls": 1, "economy": 0 };
      }
    }

  } //for end



  var result=  Object.entries(obj).sort((a, b) => b[1].economy - a[1].economy).reverse().slice(0, 10);  //important to sort and give back only 10bowlers
  
  var final= Object.fromEntries(result);

  return final;
  

}

module.exports.topBowlersGraph=topBowlersGraph;


//--------------------------------------------------------------------------------------------------------