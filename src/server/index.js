
const fs = require('fs');          //node.js module
const path = require("path");      // node.js module

var receive = require('./ipl.js');    //importing my functions

var i;


const csvFilePath = path.resolve(__dirname, "../data/matches.csv");
const csv = require('csvtojson')
csv()
    .fromFile(csvFilePath)
    .then((jsonObj) => {

        i = jsonObj;


        //Dumping Function 1

        const obj1 = receive.matches(jsonObj);
        var filePath1 = path.resolve(__dirname, "../output/matchesPerYear.json");
        writeJson(filePath1, obj1);


        //Dumping Function 2

        const obj2 = receive.matchesWon(jsonObj);
        var filePath2 = path.resolve(__dirname, "../output/matchesWonPerTeam.json");
        writeJson(filePath2, obj2);


        //Dumping Function 5

        const obj5 = receive.tossAndWon(jsonObj);
        var filePath5 = path.resolve(__dirname, "../output/tossAndWon.json");
        writeJson(filePath5, obj5);



        //Dumping Function 6

        const obj6 = receive.Player(jsonObj);

        var filePath6 = path.resolve(__dirname, "../output/Player.json");
        writeJson(filePath6, obj6);



    }) //jsonObj scope end




const csvFilePath2 = path.resolve(__dirname, "../data/deliveries.csv");
const csv2 = require('csvtojson')
csv()
    .fromFile(csvFilePath2)
    .then((deliveries_array) => {

        //Dumping Function-3

        var year1 = 2016;
        var obj3 = receive.extra_runs(i, deliveries_array, year1);
        var filePath3 = path.resolve(__dirname, "../output/extra_runs.json");
        writeJson(filePath3, obj3);




        //Dumping Function-4

        var year2 = 2015;
        var obj4 = receive.economyBowlers(i, deliveries_array, year2);

        var filePath4 = path.resolve(__dirname, "../output/topEconomyBowlers.json");
        writeJson(filePath4, obj4);


        //Dumping Function-7

        var obj7 = receive.strikeRate(i, deliveries_array);
        var filePath7 = path.resolve(__dirname, "../output/strikeRate.json");
        writeJson(filePath7, obj7);


        //Dumping Function-8


        var obj8 = receive.dismissed(deliveries_array);
        var filePath8 = path.resolve(__dirname, "../output/dismissed.json");
        writeJson(filePath8, obj8);


        // Dumping Function-9
        var obj9 = receive.superOverEconomy(deliveries_array);
        var filePath9 = path.resolve(__dirname, "../output/superOverEconomy.json");
        writeJson(filePath9, obj9);





    })//deliveries_array scope end   





function writeJson(filePath, FileName) {
    fs.writeFileSync(filePath, JSON.stringify(FileName, null, 2), (err) => {
        if (err) console.log(err)
    })

}




//Remember: This is json => array of objects.
//It is called json object because array is also an object.
//My convertor is converting .csv file to array having objects with key & values both as strings. 
//So i will have to use parseInt() to get values as numbers.

//https://www.npmjs.com/package/csvtojson
//https://github.com/Keyang/node-csvtojson (scroll down and read)