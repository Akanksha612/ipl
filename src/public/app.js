

fetch('./output/matchesPerYear.json')    
    .then(response => response.json())
    .then((data1) => generateGraph1(data1));
    
    
  function generateGraph1(data1)
  {
    Highcharts.chart('container1', {
        chart: {
          type: 'line'
        },
        title: {
          text: "Matches per year"
        },
        
        xAxis: {
          categories: Object.keys(data1)
        },

        yAxis: {
            
           text: "Matches"
          
        },

        series: [{
          name: 'Matches',
          data: Object.values(data1)
      
        }]
      });  //highcharts end

  }//function end


//----------------------------------------------------------------------------------------------------------


fetch('./output/matchesWonPerTeam.json')
  .then(response => response.json())
  .then(data2 => generateGraph2(data2));


function generateGraph2(data2)
{
  const teams = Object.keys(data2);
  
  function Years(data2)
  {
    const years = Object.values(data2).map((data2) => Object.keys(data2));
    const yearsArray = new Set(years.flat());

    let Array =[];
    for( let item of yearsArray)
    {
      Array.push(parseInt(item));
    }
    return Array.sort();
  }
  const uniqueYears = Years(data2)

  function result(data2,uniqueYears){

    const allTeams = Object.keys(data2);
    const arr = []
    for ( let team of allTeams){

      const presentObject = {};

      presentObject.name = team;
      presentObject['data'] = [];

      for (let index = 0 ; index < uniqueYears.length ; index++)
      {
        if(data2.hasOwnProperty(team))
        {
          if(data2[team].hasOwnProperty(uniqueYears[index])){
            presentObject['data'].push(data2[team][uniqueYears[index]]);
          }else
          {
            presentObject['data'].push(0);
          }
      }
    }
    arr.push(presentObject);
  }
  return arr; 
};

  const final = result(data2,uniqueYears)

  Highcharts.chart('container2', {
    chart: {
      type: 'column'
    },
    title: {
      text: 'Number Of Wins Per Team Per Year'
    },
    subtitle: {
      text: 'IPL Project'
    },
    xAxis: {
      categories: uniqueYears
    },
    yAxis: {
      title: {
        text: 'Number of Wins Per Team Per Year'
      }
    },
    series: final
  });
}
//-----------------------------------------------------------------------------------------



fetch('./output/extra_runs.json')           
    .then(response => response.json())
    .then((data3) => generateGraph3(data3));
    
    
  function generateGraph3(data3)
  {
    Highcharts.chart('container3', {
        chart: {
          type: 'bar'
        },
        title: {
          text: "Extra_runs per team in 2016"
        },
        
        xAxis: {
          categories: Object.keys(data3)
        },

        yAxis: {
            
           text: "Extra_runs"
          
        },

        series: [{
          name: 'Extra_runs',
          data: Object.values(data3)
      
        }]
      });  //highcharts end

  }//function end


//---------------------------------------------------------------------------------------------------



  fetch('./output/topBowlersGraph.json')   
    .then(response => response.json())
    .then((data4) => generateGraph4(data4));
    
    
  function generateGraph4(data4)
  {

    var arr=[];
     
    for(var prop in data4)
    {
      arr.push(Number(data4[prop].economy));
    }


    Highcharts.chart('container4', {
        chart: {
          type: 'column'
        },
        title: {
          text: "Top Economy Bowlers in 2015"
        },
        
        xAxis: {
          categories: Object.keys(data4)
        },

        yAxis: {
            
           text: "Economy"
          
        },

        series: [{
          name: 'Economy',
          data: arr
      
        }]
      });  //highcharts end

  }//function end

  

 

 